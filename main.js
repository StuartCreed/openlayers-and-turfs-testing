import 'ol/ol.css';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/Map';
import View from 'ol/View';
import { Fill, Stroke, Style} from 'ol/style';
import {Vector as VectorSource} from 'ol/source';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import BingMaps from "ol/source/BingMaps";

/*

Logic thoughts (IN PROGRESS):

if (influence < 5%) {
    DON'T VIEW TITLE
}
else {
    if (intersection area percentage relative to title area > 5%) {
        VIEW TITLE
    }
    else {
        DON'T VIEW TITLE
    }
}

-> it is mainly an issue when there is a title a lot bigger than a site, this logic resolves this.

 */


///////////
var site = turf.polygon([[[-4600000, -500000], [-4600000, 400000], [-3800000, 400000], [-3800000, -500000], [-4600000, -500000]]], { name: 'site' });
var site_area = turf.area(site)*1000000000000;  // turf changes the units from m squared to m squared * 1000000000000 after a certain point
console.log('site_area in square meters', site_area);
var siteSource = new VectorSource({
    features: new GeoJSON().readFeatures(site),
});
var siteLayer = new VectorLayer({
    source: siteSource,
    style: new Style({
        stroke: new Stroke({
            color: 'red'
        })
    }),
});
///////////


//////////////////

///////////
var polygon1 = turf.polygon([[[-5500000, -350000], [-5500000, 250000], [-4200000, 320000], [-4200000, -350000], [-5500000, -350000]]], { name: 'polygon1' });
var polygon1_area = turf.area(polygon1);
console.log('polygon1_area in square meters', polygon1_area);
var polygon1Source = new VectorSource({
    features: new GeoJSON().readFeatures(polygon1),
});
var polygon1Layer = new VectorLayer({
    source: polygon1Source,
    style: new Style({
        stroke: new Stroke({
            color: 'green'
        })
    }),
});
///////////

///////////
var intersection_site_polygon1 = turf.intersect(site, polygon1);
var intersection_site_polygon1_area = turf.area(intersection_site_polygon1);
console.log('intersection_site_polygon1_area in square meters', intersection_site_polygon1_area);
var intersection_site_polygon1_area_percentage = (intersection_site_polygon1_area/polygon1_area)*100;
console.log('intersection_site_polygon1_area_percentage', intersection_site_polygon1_area_percentage);
let influence_of_polygon1_on_site = (intersection_site_polygon1_area/site_area)*100;
console.log('influence_of_polygon1_on_site', influence_of_polygon1_on_site);
var intersection_site_polygon1_Source = new VectorSource({
    features: new GeoJSON().readFeatures(intersection_site_polygon1),
});
var intersection_site_polygon1_Layer = new VectorLayer({
    source: intersection_site_polygon1_Source,
    style: new Style({
        fill: new Fill({
            color: 'pink',
        }),
        stroke: new Stroke({
            color: 'pink'
        }),
    }),
});
///////////

///////////////////////


///////////////////////

///////////
var polygon2 = turf.polygon([[[-4500000, -450000], [-4500000, 350000], [-4000000, 360000], [-4000000, -350000], [-4500000, -450000]]], { name: 'polygon2' });
var polygon2_area = turf.area(polygon2);
console.log('polygon2_area in square meters', polygon2_area);
var polygon2Source = new VectorSource({
    features: new GeoJSON().readFeatures(polygon2),
});
var polygon2Layer = new VectorLayer({
    source: polygon2Source,
    style: new Style({
        stroke: new Stroke({
            color: 'blue'
        })
    }),
});
///////////

///////////
var intersection_site_polygon2 = turf.intersect(site, polygon2);
var intersection_site_polygon2_area = turf.area(intersection_site_polygon2);
console.log('intersection_site_polygon2_area in square meters', intersection_site_polygon2_area);
var intersection_site_polygon2_area_percentage = (intersection_site_polygon2_area/polygon2_area)*100;
console.log('intersection_site_polygon2_area_percentage', intersection_site_polygon2_area_percentage);
var intersection_site_polygon2_Source = new VectorSource({
    features: new GeoJSON().readFeatures(intersection_site_polygon2),
});
var intersection_site_polygon2_Layer = new VectorLayer({
    source: intersection_site_polygon2_Source,
    style: new Style({
        fill: new Fill({
            color: 'blue',
        }),
        stroke: new Stroke({
            color: 'blue'
        }),
    }),
});
///////////

///////////////////////
//////////////////////

let layersArray = [
    new TileLayer({
        source: new BingMaps({
            key: 'AsND20gAeOw3b6G8k0-LKAAo870HkRIVjDGPzocnJ6CXy3cwC3znbnOX-VAGVd_i', //Your Bing Maps Key from http://www.bingmapsportal.com/ here
            imagerySet: 'Aerial',
        }),
    }),
    siteLayer, polygon1Layer, polygon2Layer];

///////////

var map = new Map({
    layers: layersArray,
    target: 'map',
    view: new View({
        center: [0, 0],
        zoom: 4.5,
    }),
});