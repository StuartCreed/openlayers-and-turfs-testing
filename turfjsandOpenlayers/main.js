import 'ol/ol.css';
import Circle from 'ol/geom/Circle';
import Feature from 'ol/Feature';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/Map';
import View from 'ol/View';
import { Fill, Stroke, Style} from 'ol/style';
import {Vector as VectorSource} from 'ol/source';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import BingMaps from "ol/source/BingMaps";
import Contains from 'ol/format/filter/Contains';

var geojsonObject = {
    'type': 'FeatureCollection',
    'crs': {
        'type': 'name',
        'properties': {
            'name': 'EPSG:3857',
        },
    },
    'features': [
        {
            'type': 'Feature',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [
                        [-5e6, -1e6],
                        [-4e6, 1e6],
                        [-3e6, -1e6] ] ],
            },
        },
        {
            'type': 'Feature',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [
                        [-4.5e6, -0.8e6],
                        [-4e6, 0.6e6],
                        [-3.5e6, -0.8e6] ] ],
            },
        },
    ],
};

var bbox = [-9.5e6, 3.0e6 ,-8.5e6, 4.0e6];
var cellSide = 50;

var poly = turf.polygon([[[0,29],[3.5,29],[2.5,32],[0,29]]]);
var options = {pivot: [0, 25]};
var scaledPoly = turf.transformScale(poly, 1000000);
var rotatedPoly = turf.transformRotate(scaledPoly, 10, options);

var triangleGrid = turf.triangleGrid(bbox, cellSide);
var gridSource = new VectorSource({
    features: new GeoJSON().readFeatures(triangleGrid),
});

var polySource = new VectorSource({
    features: new GeoJSON().readFeatures(rotatedPoly),
});

var vectorSource = new VectorSource({
    features: new GeoJSON().readFeatures(geojsonObject),
});

var vectorLayer = new VectorLayer({
    source: vectorSource,
    style: new Style({
        stroke: new Stroke({
            color: 'blue'
        })
    }),
});

var gridLayer = new VectorLayer(({
    source: gridSource,
    style: new Style({
        stroke: new Stroke({
            color: 'red'
        })
    }),
}))

var polyLayer = new VectorLayer(({
    source: polySource,
    style: new Style({
        stroke: new Stroke({
            color: 'green'
        })
    }),
}))


var map = new Map({
    layers: [
        new TileLayer({
            source: new BingMaps({
                key: 'AsND20gAeOw3b6G8k0-LKAAo870HkRIVjDGPzocnJ6CXy3cwC3znbnOX-VAGVd_i', //Your Bing Maps Key from http://www.bingmapsportal.com/ here
                imagerySet: 'Aerial',
            }),
        }),
        vectorLayer, gridLayer, polyLayer],
    target: 'map',
    view: new View({
        center: [0, 0],
        zoom: 2,
    }),
});