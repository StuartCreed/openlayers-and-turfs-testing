import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import {Draw, Modify, Snap} from 'ol/interaction';
import {OSM, Vector as VectorSource} from 'ol/source';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import GeoJSON from 'ol/format/GeoJSON';

var raster = new TileLayer({
    source: new OSM(),
});

var source = new VectorSource();
var vector = new VectorLayer({
    source: source,
    style: new Style({
        fill: new Fill({
            color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
            color: '#ffcc33',
            width: 2,
        }),
        image: new CircleStyle({
            radius: 7,
            fill: new Fill({
                color: '#ffcc33',
            }),
        }),
    }),
});

var map = new Map({
    layers: [raster, vector],
    target: 'map',
    view: new View({
        center: [-11000000, 4600000],
        zoom: 4,
    }),
});

const draw = new Draw({
    source: source,
    type: 'Circle',
});
map.addInteraction(draw);
draw.on("drawend",function(e){
    let drawnPolygon = e.feature;
    console.log('e.feature is', drawnPolygon);
    console.log('Geometry is', drawnPolygon.getGeometry());
    console.log('coordinates are', drawnPolygon.getGeometry().flatCoordinates);
    let drawnPolygonCoords = drawnPolygon.getGeometry().flatCoordinates;
    map.removeInteraction(draw);
})








